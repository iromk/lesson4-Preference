package com.geekbrains.sharedpreferencesexample;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final String TEST_PREFERENCES = "TestPreferences";
    public static final String DEFAULT_PREFERENCES = "";
    private Button prefButton;
    private Button sharedPrefButton;
    private Button removeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadPrefs();
        loadSharedPrefs();

        initUi();

        addAllListenersToButtons();
    }

    private void addAllListenersToButtons() {
        prefButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // подключаемся к SharedPreferences в режиме MODE_PRIVATE
                // т.е. к файлу будет доступ только у нашего приложения
                // существуют еще два режима: MODE_WORLD_READABLE и MODE_WORLD_WRITABLE
                // при которых данные будут доступны другим приложениям, которым известен идентификатор файла
                SharedPreferences pref = getPreferences(Context.MODE_PRIVATE);
                // метод getPreferences открывает файл с именем текущего активити
                // в данном случае файл будет называться MainActivity.xml

                // сохраняем
                saveToSharedPreferences(pref);
                // загружаем из SharedPreferences в поле R.id.textViewPref
                loadPrefs();
            }
        });


        sharedPrefButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // подключаемся к SharedPreferences в режиме MODE_PRIVATE
                // т.е. к файлу будет доступ только у нашего приложения
                // существуют еще два режима: MODE_WORLD_READABLE и MODE_WORLD_WRITABLE
                // при которых данные будут доступны другим приложениям, которым известен идентификатор файла
                SharedPreferences sharedPref = getSharedPreferences(TEST_PREFERENCES, Context.MODE_PRIVATE);
                // метод getSharedPreferences подключается к файлу имя которого задается в первом параметре
                // в данном случае файл будет называться TestPreferences.xml

                // сохраняем
                saveToSharedPreferences(sharedPref);

                // загружаем из SharedPreferences в поле R.id.textViewSharedPref
                loadSharedPrefs();
            }
        });

        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePrefs();
                removeSharedPrefs();
                updateAllPrefs();
            }
        });
    }

    private void initUi() {
        prefButton = (Button) findViewById(R.id.buttonPref);
        sharedPrefButton = (Button) findViewById(R.id.buttonSharedPref);
        removeButton = (Button) findViewById(R.id.buttonRemove);
    }

    private void saveToSharedPreferences(SharedPreferences sharedPref) {
        EditText editText = (EditText) findViewById(R.id.editText);

        if (!TextUtils.isEmpty(editText.getText().toString())) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(getString(R.string.shared_prefs_key_text), editText.getText().toString());
            editor.apply();
        }

        editText.setText("");
    }

    private void loadPrefs() {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        // загружаем из SharedPreferences значение нашего ключа
        String loadedPrefs = sharedPref.getString(getString(R.string.shared_prefs_key_text), DEFAULT_PREFERENCES);
        // записываем значение в TextView
        TextView textViewPrefs = (TextView) findViewById(R.id.textViewPref);
        String s = getResources().getString(R.string.pref) + ":" + loadedPrefs;
        textViewPrefs.setText(s);
    }


    private void removePrefs() {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        // загружаем из SharedPreferences значение нашего ключа
        // записываем значение в TextView
        sharedPref.edit().remove(getString(R.string.shared_prefs_key_text)).apply();
    }

    private void removeSharedPrefs() {
        SharedPreferences sharedPref = getSharedPreferences(TEST_PREFERENCES, Context.MODE_PRIVATE);
        // загружаем из SharedPreferences значение нашего ключа
        // записываем значение в TextView
        sharedPref.edit().remove(getString(R.string.shared_prefs_key_text)).apply();
    }


    private void loadSharedPrefs() {
        SharedPreferences sharedPref = getSharedPreferences(TEST_PREFERENCES, Context.MODE_PRIVATE);
        // загружаем из SharedPreferences значение нашего ключа
        String loadedPrefs = sharedPref.getString(getString(R.string.shared_prefs_key_text), DEFAULT_PREFERENCES);
        // записываем значение в TextView
        TextView textViewPrefs = (TextView) findViewById(R.id.textViewSharedPref);
        String text = getResources().getString(R.string.shared_pref) + ":" + loadedPrefs;
        textViewPrefs.setText(text);
    }

    private void updateAllPrefs() {
        loadPrefs();
        loadSharedPrefs();
    }

}
